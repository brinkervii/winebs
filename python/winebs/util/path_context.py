from pathlib import Path
import os


class PathContext:
    path: Path
    pwd: Path

    def __init__(self, path: Path):
        assert path.exists()
        self.path = path
        self.pwd = Path("..").resolve()

    def __enter__(self):
        self.pwd = Path("..").resolve()
        os.chdir(self.path)

    def __exit__(self, *_):
        os.chdir(self.pwd)


def cd(path: Path):
    return PathContext(path)
