import io
import re
import subprocess
from dataclasses import dataclass
from pathlib import Path
from typing import Optional, List, Dict

from winebs.util.path_context import cd


def build(dockerfile: Path, name: str, tag: str):
    with cd(dockerfile.parent):
        subprocess.check_call([
            "docker", "build",
            "--rm",
            "-t", f"{name}:{tag}",
            str(dockerfile.parent)
        ])


@dataclass
class ImageListingItem:
    repository: str
    tag: str
    image: str


@dataclass
class ImageListing:
    items: List[ImageListingItem]

    def find(self, image: str, tag: str) -> Optional[ImageListingItem]:
        for item in self.items:
            if item.image == image and item.tag == tag:
                return item

        return None


def image_ls() -> List[ImageListingItem]:
    header: Optional[List[str]] = None
    lines: List[List[str]] = []
    big_list: List[Dict[str, str]] = []

    with io.BytesIO(subprocess.check_output(["docker", "image", "ls"])) as buf:
        with io.TextIOWrapper(buf) as fp:
            while line := fp.readline():
                line = line.strip()
                if not line:
                    continue

                line_items = [x.strip() for x in re.split(r"\s+", line)]

                if header is None:
                    header = list(map(str.lower, line_items))

                else:
                    lines.append(line_items)

    for ln in lines:
        my_dict = {}
        for i, item in enumerate(ln):
            try:
                key = header[i]

            except IndexError:
                continue

            my_dict[key] = item

        big_list.append(my_dict)

    return ImageListing(
        [ImageListingItem(repository=x["repository"], tag=x["tag"], image=x["image"]) for x in big_list]
    )
