import logging
import subprocess
import sys
from pathlib import Path

from winebs.util.path_context import cd

log = logging.getLogger(__name__)


def clone(repository: str, target_path: Path):
    log.info(f"Cloning {repository} into {target_path}")

    with cd(target_path.parent):
        subprocess.check_call(["git", "clone", repository, str(target_path)])

    assert target_path.is_dir()


def checkout(repository_path: Path, revision: str):
    log.info(f"Checkout out repository {repository_path} with revision {reversed}")

    with cd(repository_path):
        subprocess.check_call(["git", "checkout", revision])


def pull(repository_path: Path):
    log.info(f"Running git pull for {repository_path}")
    with cd(repository_path):
        subprocess.check_call(["git", "pull"])


def short_hash(repository_path: Path) -> str:
    with cd(repository_path):
        res = subprocess.check_output(["git", "rev-parse", "--short", "HEAD"])

    short_hash = res.decode(sys.getdefaultencoding()).strip().lower()
    log.info(f"Short hash for {repository_path} is {short_hash}")

    return short_hash
