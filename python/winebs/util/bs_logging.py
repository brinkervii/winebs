import logging
import sys


def configure():
    logging.basicConfig(
        format="[%(levelname)s] [%(asctime)s] %(name)s:: %(message)s",
        datefmt="%Y-%m-%d %H:%M:%S",
        level=logging.INFO,
        stream=sys.stderr,
    )
