import winebs.util.bs_logging as bs_logging
from winebs.cli import cli

bs_logging.configure()

if __name__ == "__main__":
    cli()
