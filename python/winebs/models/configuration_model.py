from __future__ import annotations

from pathlib import Path
from typing import Optional

import tomlkit
from pydantic import BaseModel


class BuildSystem(BaseModel):
    build_title: str
    syswow64: bool


class Wine(BaseModel):
    repository: str
    version: Optional[str] = ""


class WineStaging(BaseModel):
    repository: str
    version: Optional[str] = ""
    enabled: bool


class ConfigurationModel(BaseModel):
    bs: BuildSystem
    wine: Wine
    wine_staging: WineStaging

    @classmethod
    def from_file(cls, path: Path):
        with path.open("rb") as fp:
            model = cls.model_validate(tomlkit.load(fp))

        return model
