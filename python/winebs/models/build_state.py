from __future__ import annotations

from dataclasses import dataclass
from pathlib import Path

from winebs.models.configuration_model import ConfigurationModel
from winebs.util import git


@dataclass
class BuildState:
    project_directory: Path
    state_directory: Path
    configuration: ConfigurationModel
    git_hash: str

    @property
    def wine_path(self) -> Path:
        return self.state_directory / "wine"

    @property
    def wine_staging_path(self) -> Path:
        return self.state_directory / "wine-staging"

    @property
    def docker_images_root(self):
        return self.project_directory / "docker"

    @classmethod
    def new(cls, configuration_path: Path) -> BuildState:
        state = BuildState(
            project_directory=configuration_path.parent,
            state_directory=configuration_path.parent / "state",
            configuration=ConfigurationModel.from_file(configuration_path),
            git_hash=git.short_hash(configuration_path.parent)
        )

        assert state.project_directory.is_dir()
        state.state_directory.mkdir(parents=True, exist_ok=True)

        return state
