import subprocess
from dataclasses import dataclass, field
from typing import List, Tuple


@dataclass
class DockerRunConfiguration:
    image: str
    tag: str

    volumes: List[Tuple[str, str]] = field(default_factory=list)
    command: List[str] = field(default_factory=lambda: ["/bin/bash -c 'echo No command set'"])

    def volume(self, host_path: str, container_path: str):
        self.volumes.append((host_path, container_path))

    @property
    def volume_args(self) -> List[List[str]]:
        arg_list = []

        for h, c in self.volumes:
            arg_list.append(["-v", f"{h}:{c}"])

        return arg_list

    def use_command(self, command: List[str]):
        self.command = [*command]

    def run(self):
        cmd = ["docker", "run", "--rm"]

        if self.volumes:
            for volume_args in self.volume_args:
                cmd.extend(volume_args)

        cmd.append("-it")
        cmd.append(f"{self.image}:{self.tag}")

        cmd.extend(self.command)

        subprocess.check_call(cmd)
