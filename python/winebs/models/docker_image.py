import logging
from dataclasses import dataclass
from pathlib import Path

from winebs.models.docker_run_configuration import DockerRunConfiguration
from winebs.util import docker

log = logging.getLogger(__name__)


@dataclass
class DockerImage:
    path: Path
    tag: str

    @property
    def name(self):
        return self.path.name

    @property
    def dockerfile(self):
        dockerfile_path = self.path / "Dockerfile"
        assert dockerfile_path.exists(), "Dockerfile does not exist"

        return dockerfile_path

    def build(self):
        log.info(f"Building docker image {repr(self)}")
        docker.build(
            self.dockerfile,
            self.name,
            self.tag
        )

    def run(self) -> DockerRunConfiguration:
        return DockerRunConfiguration(image=self.name, tag=self.tag)
