import logging
from dataclasses import dataclass
from pathlib import Path

import winebs.util.git as git
from winebs.models.build_state import BuildState

log = logging.getLogger(__name__)


@dataclass
class CloneWineResult:
    path: Path
    did_clone: bool


def clone_wine(state: BuildState):
    if state.wine_path.exists():
        log.info(f"{state.wine_path} already exists")
        assert state.wine_path.is_dir()
        return CloneWineResult(path=state.wine_path, did_clone=False)

    git.clone(state.configuration.wine.repository, state.wine_path)

    return CloneWineResult(path=state.wine_path, did_clone=True)


@dataclass
class CheckoutWineVersionResult:
    did_checkout: bool


def checkout_wine_version(state: BuildState, repository_path: Path):
    if isinstance(state.configuration.wine.version, str) and state.configuration.wine.version.strip():
        log.info(f"Checking out wine version {state.configuration.wine.version}")
        git.checkout(repository_path, state.configuration.wine.version)
        return CheckoutWineVersionResult(did_checkout=True)

    log.info(f"No wine version pinned")

    return CheckoutWineVersionResult(did_checkout=False)


@dataclass
class CloneWineStagingResult:
    path: Path
    did_clone: bool


def clone_wine_staging(state: BuildState):
    assert state.configuration.wine_staging.enabled, "Wine staging is not enabled for this build"

    if state.wine_staging_path.exists():
        log.info(f"{state.wine_staging_path} already exists")
        assert state.wine_staging_path.is_dir()
        return CloneWineResult(path=state.wine_staging_path, did_clone=False)

    git.clone(state.configuration.wine_staging.repository, state.wine_staging_path)

    return CloneWineStagingResult(path=state.wine_staging_path, did_clone=True)


@dataclass
class CheckoutWineStagingVersionResult:
    did_checkout: bool


def checkout_wine_staging_version(state: BuildState, path: Path):
    if isinstance(state.configuration.wine_staging.version, str) and state.configuration.wine_staging.version.strip():
        log.info(f"Checking out wine-staging version {state.configuration.wine_staging.version}")
        git.checkout(path, state.configuration.wine_staging.version)
        return CheckoutWineStagingVersionResult(did_checkout=True)

    log.info(f"No wine-staging version pinned")

    return CheckoutWineStagingVersionResult(did_checkout=False)
