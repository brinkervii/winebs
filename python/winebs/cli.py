import subprocess
from pathlib import Path
from typing import Optional

import click

import winebs.tasks as tasks
from winebs.models.build_state import BuildState
from winebs.models.docker_image import DockerImage
from winebs.util import git
from winebs.util.path_context import cd

file = Path(__file__).resolve()
here = file.parent
cwd = Path(".")


@click.group()
def cli():
    ...


def default_configuration_path() -> Path:
    return Path("configuration.toml").resolve()


@click.option("--configuration", type=Path)
@cli.command()
def build(configuration: Optional[Path] = None):
    configuration = configuration or default_configuration_path()

    state = BuildState.new(configuration)

    clone_wine_result = tasks.clone_wine(state)
    checkout_wine_result = tasks.checkout_wine_version(state, clone_wine_result.path)
    if not checkout_wine_result.did_checkout:
        git.pull(clone_wine_result.path)

    clone_wine_staging_result = tasks.clone_wine_staging(state)
    checkout_wine_staging_result = \
        tasks.checkout_wine_staging_version(state, clone_wine_staging_result.path)
    if not checkout_wine_staging_result.did_checkout:
        git.pull(clone_wine_staging_result.path)

    tag_path = Path(state.docker_images_root / "tag.txt")
    with tag_path.open("r") as fp:
        tag = fp.readline().strip()

    wine64_docker_image = DockerImage(state.docker_images_root / "winebs-build-wine64", tag)
    wine64_docker_image.build()

    wine32_docker_image = DockerImage(state.docker_images_root / "winebs-build-wine32", tag)
    wine32_docker_image.build()

    wine64_run = wine64_docker_image.run()
    wine64_run.volume(str(state.project_directory), "/winebs")
    wine64_run.use_command(["/bin/bash", "-c", "/winebs/winebs-docker docker build wine64"])
    wine64_run.run()

    wine32_run = wine32_docker_image.run()
    wine32_run.volume(str(state.project_directory), "/winebs")
    wine32_run.use_command(["/bin/bash", "-c", "/winebs/winebs-docker docker build wine32"])
    wine32_run.run()

    dist_run = wine64_docker_image.run()
    dist_run.volume(str(state.project_directory), "/winebs")
    dist_run.use_command(["/bin/bash", "-c", "/winebs/winebs-docker docker dist"])
    dist_run.run()


@cli.group()
def docker():
    ...


@docker.group(name="build")
def docker_build():
    ...


@docker_build.command(name="wine64")
def docker_build_wine64():
    state_path = Path("./state").resolve()
    wine_source = state_path / "wine"
    configure = wine_source / "configure"

    build_dir = state_path / "build-wine64"
    build_dir.mkdir(parents=True, exist_ok=True)

    with cd(build_dir):
        subprocess.check_call([str(configure), "--enable-win64"])
        subprocess.check_call(["make", "-j12"])


@docker_build.command(name="wine32")
def docker_build_wine32():
    state_path = Path("./state").resolve()
    wine_source = state_path / "wine"
    configure = wine_source / "configure"

    build_dir_wine32 = state_path / "build-wine32"
    build_dir_wine32.mkdir(parents=True, exist_ok=True)

    with cd(build_dir_wine32):
        subprocess.check_call([str(configure)])
        subprocess.check_call(["make", "-j12"])

    build_dir_syswow64 = state_path / "build-wine-syswow64"
    build_dir_syswow64.mkdir(parents=True, exist_ok=True)

    with cd(build_dir_syswow64):
        build_dir_wine64 = state_path / "build-wine64"

        subprocess.check_call([
            str(configure),
            f"--with-wine64={build_dir_wine64}",
            f"--with-wine-tools={build_dir_wine32}"
        ])

        subprocess.check_call(["make", "-j12"])


@docker.command("dist")
def docker_dist():
    state_path = Path("./state").resolve()

    build_dir_wine32 = state_path / "build-wine-syswow64"
    build_dir_wine64 = state_path / "build-wine64"

    dist_dir = state_path / "dist-wine-syswow64"
    dist_dir.mkdir(parents=True, exist_ok=True)

    with cd(build_dir_wine32):
        subprocess.check_call(
            ["make", "install"],
            env={"DESTDIR": str(dist_dir)}
        )

    with cd(build_dir_wine64):
        subprocess.check_call(
            ["make", "install"],
            env={"DESTDIR": str(dist_dir)}
        )
