venv:
	python -m venv ./venv
	./venv/bin/python -m pip install --upgrade pip
	./venv/bin/python -m pip install -r requirements.txt

install-requirements:
	./venv/bin/python -m pip install --upgrade -r requirements.txt

format-code:
	./venv/bin/python -m black python -l 120

.PHONY: install-requirements format-code